package com.example.mybatisdemo.repo;

import com.example.mybatisdemo.model.Employee;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@MybatisTest
class EmployeeMapperTest {
  @Autowired
  EmployeeMapper employeeMapper;

  @Test
  public void savesAnEmployee() {
    Employee employee = new Employee("Fred Bloggs", "12345");
    employeeMapper.save(employee);
    assertNotNull(employee.getId());
  }
}