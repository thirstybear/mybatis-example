package com.example.mybatisdemo.repo;

import com.example.mybatisdemo.model.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

@Mapper
public interface EmployeeMapper {
  @Options(useGeneratedKeys = true, keyProperty = "id")
  @Insert("insert into employee (name, payrollref) values (#{name}, #{payrollref})")
  public void save(Employee employee);
}
