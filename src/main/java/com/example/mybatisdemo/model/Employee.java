package com.example.mybatisdemo.model;

public class Employee {
  private Long id;
  private String name;
  private String payrollref;

  public Employee(String name, String payrollref) {
    this.name = name;
    this.payrollref = payrollref;
  }

  public String getName() {
    return name;
  }

  public String getPayrollref() {
    return payrollref;
  }

  public Long getId() {
    return id;
  }
}
