package com.example.mybatisdemo;

import com.example.mybatisdemo.repo.EmployeeMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

  private EmployeeMapper employeeMapper;

  public Application(EmployeeMapper employeeMapper) {
    this.employeeMapper = employeeMapper;
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
