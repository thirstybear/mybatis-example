# MyBatis Example
Noodling with MyBatis and SpringBoot, _avoiding_ XML.

## Why?
A few reasons.

1. XML tends to obfuscate configuration, and even _override_ functionality in ways invisible to IDEs. Which is Bad Karma.
2. The vast majority of "annotation-based" code samples still seem to sneak in some XML config.

## Things to note
* You _can_ write a MyBatis data layer without XML! 
* Spring & Spring Boot does most the heavy lifting for you in terms of Session and Factory objects.
* MyBatis does **_not_** generate primary keys (unlike JPA libraries). It relies on the database auto-incrementing the primary key.
* MyBatis does integrate with the SpringBoot data parameters in `application.properties`
* In general this framework is _much_ more fragile than, say Spring Boot & JPA.

## What next?
* Compare and contrast with a similar simple app using JPA or Spring Data
